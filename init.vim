set number
set noswapfile
set relativenumber
set guicursor=
set nowrap
set noerrorbells
set nohlsearch
set noswapfile
set clipboard+=unnamedplus
set incsearch
set scrolloff=8
set signcolumn=yes
set colorcolumn=120

call plug#begin('~/.config/nvim/plugins')
Plug 'tpope/vim-fugitive'
Plug 'vim-airline/vim-airline'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
call plug#end()

inoremap <c-j> <down>
inoremap <c-k> <up>
